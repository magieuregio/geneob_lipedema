#!/usr/bin/python3

#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt2
import math
import seaborn as sns
import csv
import scipy.stats as stats
import statsmodels.api as sm
from bioinfokit import analys, visuz
from scipy.stats import chi2_contingency
from scipy.stats import chi2

import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json
import os

import warnings
from statsmodels.tools.sm_exceptions import ConvergenceWarning
warnings.simplefilter('ignore', ConvergenceWarning)

def import_matrix(path):
    return pd.read_csv(path, sep='\t', header=0)


def common_snp():
    #---------eliminare righe con tutti 0/0-----------------------
    c_matrix_snp_lip=matrix_snp_lip.replace('0/0', np.nan).dropna(axis=0, how='all', subset=np.array((matrix_snp_lip.columns[3:]))).fillna('0/0')
    c_matrix_snp_ob=matrix_snp_ob.replace('0/0', np.nan).dropna(axis=0, how='all', subset=np.array((matrix_snp_ob.columns[3:]))).fillna('0/0')
    c_matrix_snp_an=matrix_snp_an.replace('0/0', np.nan).dropna(axis=0, how='all', subset=np.array((matrix_snp_an.columns[3:]))).fillna('0/0')
    c_matrix_snp_an.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/CLEANED_MATRIX_SNP_ANORESSIA.csv")
    c_matrix_snp_ob.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/CLEANED_MATRIX_SNP_OBESITY.csv")
    c_matrix_snp_lip.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/CLEANED_MATRIX_SNP_LIPEDEMA.csv")

    #----------merge delle matrici per avere nella matrice finale solo gli SNP comuni
    anMob = pd.merge(c_matrix_snp_an, c_matrix_snp_ob, on='dbsnp', how='outer')
    anMobMlip = pd.merge(anMob, c_matrix_snp_lip, on='dbsnp', how='outer')

    anMlip = pd.merge(c_matrix_snp_an, c_matrix_snp_lip, on='dbsnp', how='outer')
    #----------salvare in file csv i risultati
    anMlip.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/matrix_SNP_AN_merge_LIP.csv")
    anMob.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/matrix_SNP_AN_merge_OB.csv")
    anMobMlip.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/matrix_SNP_in_common.csv")
    return anMobMlip

def final_matrix():
    #----------creazione delle 3 matrici con SNP comuni
    common_snp = pd.DataFrame([anMobMlip.dbsnp]).transpose()
    common_snp.to_csv('common_snp.csv')

    final_matrix_snp_an = pd.merge(common_snp, matrix_snp_an, on='dbsnp', how='left')
    final_matrix_snp_ob = pd.merge(common_snp, matrix_snp_ob, on='dbsnp', how='left')
    final_matrix_snp_lip = pd.merge(common_snp, matrix_snp_lip, on='dbsnp', how='left')

    final_matrix_snp_an.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/FINAL_MATRIX_SNP_ANORESSIA.csv")
    final_matrix_snp_ob.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/FINAL_MATRIX_SNP_OBESITY.csv")
    final_matrix_snp_lip.to_csv("./SNP_MATRIX/COMMON_SNP_MATRIX/FINAL_MATRIX_SNP_LIPEDEMA.csv")
    return final_matrix_snp_an, final_matrix_snp_ob, final_matrix_snp_lip, common_snp

def extract_counts_df(final_matrix_snp, name):
    #------ create a dataframe with as column '0/0', '0/1', '1/1', as row the snp, in each position can be found the number of '*/*' in that snp

    counts = {'dbsnp': common_snp['dbsnp'],
                '0/0': (final_matrix_snp[list(final_matrix_snp.columns[3:])].replace('0/0', np.nan).isnull().sum(axis=1)).tolist(),
                '0/1': (final_matrix_snp[list(final_matrix_snp.columns[3:])].replace('0/1', np.nan).isnull().sum(axis=1)).tolist(),
                '1/1': (final_matrix_snp[list(final_matrix_snp.columns[3:])].replace('1/1', np.nan).isnull().sum(axis=1)).tolist()}

    snp_count = pd.DataFrame(counts, columns = ['dbsnp','0/0','0/1', '1/1'])
    snp_count.to_csv('_'.join(['snp_count', name]))

    return snp_count, counts


def add_counts_non_zero_df(counts):
    #----- add to the dataframe counts the column of '/1' that sums for each row the value of '0/1' and '1/1'
    counts['/1'] = sum(np.array([counts['0/1'], counts['1/1']])).tolist()
    snp_count = pd.DataFrame(counts, columns = ['dbsnp','0/0','0/1', '1/1', '/1'])
    return snp_count, counts

def genotype_frequency(snp_count, name):
    print('genotype frequency')
    #----- calculate the genotype frequency as: number of samples with that genotype/number of samples

    n_sample = snp_count.iloc[1, 1] + snp_count.iloc[1, 2] + snp_count.iloc[1, 3]
    snp_genotype_freq = snp_count.iloc[:, 0:4]

    snp_genotype_freq.iloc[:, 1:4] = snp_genotype_freq.iloc[:, 1:4].astype(float).div(n_sample)
    snp_genotype_freq.columns = ['dbsnp', 'f(0/0)', 'f(0/1)', 'f(1/1)']

    snp_genotype_freq = snp_genotype_freq.set_index('dbsnp')
    snp_genotype_freq.to_csv('genotype_freq_'+name+'.csv')
    return n_sample, snp_genotype_freq

def allele_frequency(final_matrix_snp, name):
    print('allele_frequency')
    #----- calculate the allele frequency as the relative frequency of an allele:
    #all the occurrences of that allele/the total number of chromosome copies across the population

    snp = final_matrix_snp.iloc[:, 0]
    sub_final = final_matrix_snp.iloc[:, 3:]
    sub_final_snp = pd.concat([snp, sub_final], axis=1)

    sep={}

    for i in range(0,sub_final_snp.shape[0]): #per ogni rs
        snp = sub_final_snp.iloc[i,0] #rs
        sep[snp] = pd.concat([sub_final_snp.iloc[i,1:].str.split('/').str[0], sub_final_snp.iloc[i,1:].str.split('/').str[1]], ignore_index=True, sort=False) #concatena quello che c'è prima dello / e quello che c'è dopo

    counts={}
    for k in sep.keys():
        sep[k] = sep[k].replace('2', '1')
        if '0' not in sep[k].values:
            a0 = pd.Series({'0': 0})
            a1 = sep[k].value_counts()
            d0 = a0.divide(other = (sep[k].shape[0]))
            d0.index = d0.index.str.replace('0', '0/n')
            d1 = a1.divide(other = (sep[k].shape[0]))
            d1.index = d1.index.str.replace('1', '1/n')
            a = pd.concat([a0, a1])
            g = pd.concat([a, d0, d1])
        elif '1' not in sep[k].values:
            a1 = pd.Series({'1': 0})
            a0 = sep[k].value_counts()
            d0 = a0.divide(other = (sep[k].shape[0]))
            d0.index = d0.index.str.replace('0', '0/n')
            d1 = a1.divide(other = (sep[k].shape[0]))
            d1.index = d1.index.str.replace('1', '1/n')
            a = pd.concat([a0, a1])
            g = pd.concat([a, d0, d1])
        else:
            a = sep[k].value_counts()
            d0 = pd.Series(np.true_divide(a.loc['0'],sep[k].shape[0]), index = ['0/n'])
            d1 = pd.Series(np.true_divide(a.loc['1'],sep[k].shape[0]), index = ['1/n'])
            g = pd.concat([a, d0, d1])
        counts[k] = g
    c = pd.DataFrame(counts)
    c.iloc[3, :].to_csv(''.join(['allele_freq_alt_'+name]))
    c.iloc[2, :].to_csv(''.join(['allele_num_alt_'+name]))
    return counts


def check_H_W(snp_count, genotype_count, n_sample, name): #
	print('chech HWW')
	snp = [*snp_count]
	expected = {}
	observed = {}
	fisher = {}
	E00 = []
	E11 = []
	E01 = []
	O00 = []
	O11 = []
	O01 = []
	F00 = []
	F01 = []
	F11 = []
	gc = genotype_count
	for k in snp:
		c = snp_count[k]
		E00.append(c['0/n']*c['0/n'])
		E11.append(c['1/n']*c['1/n'])
		E01.append(2*c['0/n']*c['1/n'])
		d = gc.loc[k]
		#print(d)
		O00.append(d[0])
		O11.append(d[2])
		O01.append(d[1])

		F00.append(d[0] * n_sample)
		F11.append(d[2] * n_sample)
		F01.append(d[1] * n_sample)
	expected = {'dbsnp': snp, '0/0': E00, '0/1': E01, '1/1': E11}
	observed = {'dbsnp': snp, '0/0': O00, '0/1': O01, '1/1': O11}
	fisher = {'dbsnp': snp, '0/0': F00, '0/1': F01, '1/1': F11}
	exp = pd.DataFrame(expected)
	exp = exp.set_index('dbsnp')
	obs = pd.DataFrame(observed)
	obs = obs.set_index('dbsnp')
	fis = pd.DataFrame(fisher)
	fis = fis.set_index('dbsnp')

	exp.to_csv(name)
    #chi square test o fisher se ho valori bassi
	pvalue={}
	oddsratio={}
	for snp in exp.index.tolist():
		if (exp.loc[snp][0]>=5) & (exp.loc[snp][1]>=5) & (exp.loc[snp][2]>=5):
			a = ((obs.loc[snp][0] - exp.loc[snp][0])**2)/exp.loc[snp][0]
			b = ((obs.loc[snp][1] - exp.loc[snp][1])**2)/exp.loc[snp][1]
			c = ((obs.loc[snp][2] - exp.loc[snp][2])**2)/exp.loc[snp][2]
			pvalue[snp] = a+b+c
		else:
			oddsratio[snp], pvalue[snp] = stats.fisher_exact([[2*fis.loc[snp][0], fis.loc[snp][1]], [fis.loc[snp][1], 2*fis.loc[snp][2]]])

def freq_all_matrix():
    print('Entered in freq_all_matrix')
    #read data about allelic frequency and allelic number
    fral_an = pd.read_csv('allele_freq_alt_an.csv', header = None)
    fral_an = fral_an.set_index(0)

    fral_lip = pd.read_csv('allele_freq_alt_lip.csv')
    fral_lip = fral_lip.set_index('Unnamed: 0')

    nual_an = pd.read_csv('allele_num_alt_an.csv', header=None)
    nual_an = nual_an.set_index(0)

    nual_lip = pd.read_csv('allele_num_alt_lip.csv')
    nual_lip = nual_lip.set_index('Unnamed: 0')
    nual_ob = pd.read_csv('allele_num_alt_ob.csv')
    nual_ob = nual_ob.set_index('Unnamed: 0')
    #
    # ##------observed allelic frequencies
    #
    #observed genotype frequency
    #read genotype frequency
    frgen_obs_an = pd.read_csv('genotype_freq_AN.csv')
    frgen_obs_an = frgen_obs_an.set_index('dbsnp')
    frgen_obs_lip = pd.read_csv('genotype_freq_LIP.csv')
    frgen_obs_lip = frgen_obs_lip.set_index('dbsnp')
    frgen_obs_ob = pd.read_csv('genotype_freq_OB.csv')
    frgen_obs_ob = frgen_obs_ob.set_index('dbsnp')
    #
    # #expected genotype frequency
    #
    #take expected genotype frequencies
    fg_exp_an = pd.read_csv('expected_an.csv')
    fg_exp_an = fg_exp_an.set_index('dbsnp')
    fg_exp_ob = pd.read_csv('expected_ob.csv')
    fg_exp_ob = fg_exp_ob.set_index('dbsnp')
    fg_exp_lip = pd.read_csv('expected_lip.csv')
    fg_exp_lip = fg_exp_lip.set_index('dbsnp')
    #
    gnomad = pd.read_csv('snp_af_gnomad_exomes.csv')
    gnomad = gnomad.set_index('Unnamed: 0')
    gnomad_rec = gnomad*gnomad
    gnomad_rec = gnomad_rec.rename(index=str, columns={'max_maf': 'GnomAD genotype frequency'})
    gnomad_dom = (gnomad*gnomad) + (2*gnomad*(1-gnomad))
    gnomad_dom = gnomad_dom.rename(index=str, columns={'max_maf': 'GnomAD genotype frequency'})

    ####################Lipidema

    controls = pd.concat([nual_an, nual_ob], axis=1)

    c_freq = controls.sum(axis=1)
    c_freq = c_freq.divide(172)
    c_freq = c_freq.rename('Allelic frequency Controls')


    tot = pd.concat([nual_an, nual_lip, nual_ob], axis=1)
    tot_freq = tot.sum(axis=1).divide(384)
    tot_freq =tot_freq.rename('Allelic frequency')
    observed_freq = pd.concat([fral_lip, c_freq], axis=1)
    observed_tot = pd.concat([observed_freq, tot_freq], axis = 1)


    frgen_obs_lip_dom = pd.concat([frgen_obs_lip.iloc[:, 1],frgen_obs_lip.iloc[:, 2]], axis=1).sum(axis=1)
    frgen_obs_lip_dom = frgen_obs_lip_dom.rename('Observed Genotype Frequency Cases')
    frgen_obs_lip_rec = frgen_obs_lip.iloc[:, 2]
    frgen_obs_lip_rec = frgen_obs_lip_rec.rename('Observed Genotype Frequency Cases')
    frgen_obs_co_dom = pd.concat([frgen_obs_an.iloc[:, 1],frgen_obs_an.iloc[:, 2], frgen_obs_ob.iloc[:, 1], frgen_obs_ob.iloc[:, 2]], axis=1).sum(axis=1).divide(2)
    frgen_obs_co_dom = frgen_obs_co_dom.rename('Observed Genotype Frequency Controls')
    frgen_obs_co_rec = pd.concat([frgen_obs_an.iloc[:, 2], frgen_obs_lip.iloc[:, 2]], axis=1).sum(axis=1).divide(2)
    frgen_obs_co_rec = frgen_obs_co_rec.rename('Observed Genotype Frequency Controls')
    frgen_obs_dom =  pd.concat([frgen_obs_lip_dom,frgen_obs_co_dom], axis=1)
    frgen_obs_rec =  pd.concat([frgen_obs_lip_rec,frgen_obs_co_rec], axis=1)

    #take expected genotype frequency for anorexia
    fg_exp_lip_dom = pd.concat([fg_exp_lip.iloc[:, 1],fg_exp_lip.iloc[:, 2]], axis=1).sum(axis=1)
    fg_exp_lip_dom = fg_exp_lip_dom.rename('H-W Genotype Frequency Cases')
    fg_exp_lip_rec = fg_exp_lip.iloc[:, 2]
    fg_exp_lip_rec = fg_exp_lip_rec.rename('H-W Genotype Frequency Cases')

    fg_exp_co_dom = pd.concat([fg_exp_an.iloc[:, 1],fg_exp_an.iloc[:, 2], fg_exp_ob.iloc[:, 1], fg_exp_ob.iloc[:, 2]], axis=1).sum(axis=1).divide(2)
    fg_exp_co_dom = fg_exp_co_dom.rename('H-W Genotype Frequency Controls')
    fg_exp_co_rec = pd.concat([fg_exp_an.iloc[:, 2], fg_exp_ob.iloc[:, 2]], axis=1).sum(axis=1).divide(2)
    fg_exp_co_rec = fg_exp_co_rec.rename('H-W Genotype Frequency Controls')
    fg_exp_dom =  pd.concat([fg_exp_lip_dom,fg_exp_co_dom], axis=1)
    fg_exp_rec =  pd.concat([fg_exp_lip_rec,fg_exp_co_rec], axis=1)

    tot_dom = pd.concat([observed_tot,frgen_obs_dom, fg_exp_dom, gnomad_dom], axis=1, join='inner')
    tot_dom = tot_dom.rename(index=str, columns={'1/n': 'Allelic frequency Cases'})

    tot_dom.to_csv('final_freq_matrix_LIP_dom.csv')

    tot_rec = pd.concat([observed_tot,frgen_obs_rec, fg_exp_rec, gnomad_rec], axis=1, join='inner')
    tot_rec = tot_rec.rename(index=str, columns={'1/n': 'Allelic frequency Cases'})

    tot_rec.to_csv('final_freq_matrix_LIP_rec.csv')


def chi_square_test(name): #
    # # chi-squared test with similar proportions

    print('chi_square_test')
    final_matrix = pd.read_csv(name)
    final_matrix = final_matrix.set_index('Unnamed: 0')
    #print(final_matrix)
    res = {}
    for snp in final_matrix.index.tolist():
        chi={}
        #print(final_matrix.loc[snp, :])
        c_table=[	[final_matrix.loc[snp, 'Allelic frequency Cases'], final_matrix.loc[snp, 'Allelic frequency Controls'], final_matrix.loc[snp, 'Observed Genotype Frequency Cases'], final_matrix.loc[snp, 'Observed Genotype Frequency Controls'], final_matrix.loc[snp, 'H-W Genotype Frequency Cases'], final_matrix.loc[snp, 'H-W Genotype Frequency Controls'], final_matrix.loc[snp, 'H-W Genotype Frequency Cases'],final_matrix.loc[snp, 'GnomAD genotype frequency']],
        			[1.0 - final_matrix.loc[snp, 'Allelic frequency Cases'], 1.0 - final_matrix.loc[snp, 'Allelic frequency Controls'], 1.0-final_matrix.loc[snp, 'Observed Genotype Frequency Cases'], 1.0-final_matrix.loc[snp, 'Observed Genotype Frequency Controls'], 1.0-final_matrix.loc[snp, 'H-W Genotype Frequency Cases'], 1.0-final_matrix.loc[snp, 'H-W Genotype Frequency Controls'], 1.0-final_matrix.loc[snp, 'H-W Genotype Frequency Cases'], 1.0-final_matrix.loc[snp, 'GnomAD genotype frequency']]]
        c_table = np.array(c_table)
        for i in range(0,6):
            c_table[0][i] = c_table[0][i]*100
            c_table[1][i] = c_table[1][i]*100
        if np.any(c_table[:, [0,1]]==0):
            chi['Allelic frequency'] =-999
            chi['Allelic frequency pvalue'] = -999
        else:
            stat, p, dof, expected = chi2_contingency(c_table[:, [0,1]]) #stat is The test statistic, p The p-value of the test, dof id the degree of freedom, expected is the expected table calcolated
            alpha = 0.05
            chi['Allelic frequency pvalue'] = p
            if p <= alpha:
                chi['Allelic frequency'] ='Independent'
            else:
                chi['Allelic frequency'] ='Dependent'

        if np.any(c_table[:, [2,3]]==0):
            chi['Observed'] =-999
            chi['Observed pvalue'] = -999
        else:
            stat, p, dof, expected = chi2_contingency(c_table[:, [2,3]])
            alpha = 0.05
            chi['Observed pvalue'] = p
            if p <= alpha:
                chi['Observed'] ='Independent'
            else:
                chi['Observed'] ='Dependent'
        if np.any(c_table[:, [4,5]]==0):
            chi['H-W'] =-999
            chi['H-W pvalue'] = -999
        else:
            stat, p, dof, expected = chi2_contingency(c_table[:, [4,5]])
            alpha = 0.05
            chi['H-W pvalue'] = p
            if p <= alpha:
                chi['H-W'] ='Independent'
            else:
                chi['H-W'] ='Dependent'
        if np.any(c_table[:, [6,7]]==0):
            chi['H-W_gnomAD'] = -999
            chi['H-W_gnomAD pvalue'] = -999
        elif np.any(np.isnan(c_table[:, 7])):
            chi['H-W_gnomAD'] = -999
            chi['H-W_gnomAD pvalue'] = -999
        else:
            stat, p, dof, expected = chi2_contingency(c_table[:, [6,7]])
            alpha = 0.05
            chi['H-W_gnomAD pvalue'] = p
            if p <= alpha:
                chi['H-W_gnomAD'] ='Independent'
            else:
                chi['H-W_gnomAD'] ='Dependent'
        res[snp] = [chi['Allelic frequency'], chi['Observed'], chi['H-W'], chi['H-W_gnomAD'], chi['Allelic frequency pvalue'], chi['Observed pvalue'], chi['H-W pvalue'], chi['H-W_gnomAD pvalue']]
    chi_test = pd.DataFrame.from_dict(res, orient = 'index')
    chi_test.to_csv('chitest_'+name, header = ['Allelic frequency','Observed', 'H-W', 'H-W_gnomAD', 'Allelic frequency pvalue','Observed pvalue', 'H-W pvalue', 'H-W_gnomAD pvalue'])

def filter_chitest(file):
    chi_test = pd.read_csv(file+'.csv')
    chi_test = pd.concat([chi_test, ((chi_test.iloc[:,7]<0.05) & (chi_test.iloc[:,7]!=-999.0))], axis=1)
    chi_test_passed = chi_test.loc[chi_test.iloc[:,-1]==True, :]
    chi_test_passed = chi_test_passed.set_index('Unnamed: 0')
    chi_test_passed = chi_test_passed.iloc[:, :8]
    chi_test_passed.to_csv(file +'_passed.csv')


def filter_chitest_final_matrix(final, finalc1, finalc2, chitest_passed):#, name):
    print('filter_chitest_final_matrix')
    rs_passed = chitest_passed
    print(len(rs_passed), 'rs passed')
    final_passed = final[final['dbsnp'].isin(rs_passed)]
    print(final_passed.shape, 'matrix filtered')
    finalc1_passed = finalc1[finalc1['dbsnp'].isin(rs_passed)]
    finalc2_passed = finalc2[finalc2['dbsnp'].isin(rs_passed)]
    #final_passed.to_csv(name+'_'+model+'_passed.csv')
    print(finalc1_passed.shape, 'matrix filtered')
    print(finalc2_passed.shape, 'matrix filtered')
    return final_passed, finalc1_passed, finalc2_passed



def ped_map(cases, controls, name):
	#-----cases
	print('cases')
	id = cases.columns[3:].to_list()
	n_sample = len(id)
	sub_snp = pd.concat([cases.iloc[:,0],cases.iloc[:,3:]], axis=1)
	sub_snp = sub_snp.set_index('dbsnp')
	#print(sub_snp)
	sub_snp_T = sub_snp.T
	ped_cases = pd.DataFrame()
	#ped cases dataframe with A as 0 allele and B for 1 allele
	ped_cases = sub_snp_T
	#print(ped_cases)
	ped_cases = ped_cases.replace('/', ' ', regex=True)
	ped_cases = ped_cases.replace('0', 'A', regex=True)
	ped_cases = ped_cases.replace('1', 'B', regex=True)
	ped_cases = ped_cases.replace('2', 'B', regex=True)
	#print(ped_cases)
	l = [2] * ped_cases.shape[0]
	ped_cases.insert(loc=0, column='Phenotype', value=l)
	#print(ped_cases)
	#print(ped)
	ped_cases.to_csv(''.join([name, '_CASES.ped']), sep='\t', line_terminator = '\n', header = False)
	chr = cases['#CHROM'].str.split('chr').apply(lambda x: x[1])
	snp = cases['dbsnp']
	pos = cases['POS']
	data_map = [chr, snp, pos]
	data_map = pd.DataFrame(data_map)
	#print(data_map)
	data_map_cases = data_map.T
	data_map_cases = data_map_cases.set_index('#CHROM')
	data_map_cases.to_csv(''.join([name, '_CASES.map']), sep='\t', line_terminator = '\n', header = False)

	#---------------------controls
	#print('controls')
	id = controls.columns[3:].to_list()
	#print(controls)
	n_sample = len(id)
	sub_snp = pd.concat([controls.iloc[:,0],controls.iloc[:,3:]], axis=1)
	sub_snp = sub_snp.set_index('dbsnp')
	#print(sub_snp)
	sub_snp_T = sub_snp.T
	ped_controls = pd.DataFrame()
	ped_controls = sub_snp_T
	ped_controls = ped_controls.replace('/', ' ', regex=True)
	ped_controls = ped_controls.replace('0', 'A', regex=True)
	ped_controls = ped_controls.replace('1', 'B', regex=True)
	ped_controls = ped_controls.replace('2', 'B', regex=True)
	l = [1] * ped_controls.shape[0]
	ped_controls.insert(loc=0, column='Phenotype', value=l)
	#print(ped)
	ped_controls.to_csv(''.join([name, '_CONTROLS.ped']), sep='\t', line_terminator = '\n', header = False)
	chr = controls['#CHROM'].str.split('chr').apply(lambda x: x[1])
	snp = controls['dbsnp']
	pos = controls['POS']
	data_map = [chr, snp, pos]
	#print(data_map)
	data_map = pd.DataFrame(data_map)
	data_map_controls = data_map.T
	#print(data_map_controls.shape, 'map controls shape')
	data_map_controls = data_map_controls.set_index('#CHROM')
	#data_map_controls.to_csv(''.join([name, '_CONTROLS.map']), sep='\t', line_terminator = '\n', header = False)

	#--------union
	ped = pd.concat([ped_cases, ped_controls])
	print('ped')
	#ped.to_csv('.'.join([name, 'ped_no_sex']), sep='\t', line_terminator = '\n', header = False)
	ped['ID'] = ped.index
	info = pd.read_csv('patient_info.csv', sep = '\t')
	sex = info[['ID', 'Gender']]
	ped_sex = pd.merge(ped, sex, how="left")
	#print(ped_sex)
	ped_sex = ped_sex.set_index('ID')
	#print(ped_sex)
	ped_sex = ped_sex[['Gender', 'Phenotype'] + [c for c in ped_sex if c not in ['Gender', 'Phenotype']]]
	#print(ped_sex)
	ped_sex = ped_sex.replace('F', '2', regex=True)
	#print(ped_sex)
	ped_sex = ped_sex.replace('M', '1', regex=True)
	ped_sex.to_csv('.'.join([name, 'ped']), sep='\t', line_terminator = '\n', header = False)
	#map = pd.concat([data_map_cases, data_map_controls])
	map = data_map_controls
	map.to_csv('.'.join([name, 'map']), sep='\t', line_terminator = '\n', header = False)


if __name__=="__main__":

    #----- import of the SNP matrices
    matrix_snp_an = import_matrix("./SNP_MATRIX/MATRIX_SNP_ANORESSIA.csv")
    matrix_snp_ob = import_matrix("./SNP_MATRIX/MATRIX_SNP_OBESITY.csv")
    matrix_snp_lip = import_matrix("./SNP_MATRIX/MATRIX_SNP_LIPEDEMA.csv")

    # #----- find the common snp between the matrices and creation of the final matrices with just the common snp
    anMobMlip = common_snp()
    final_matrix_snp_an, final_matrix_snp_ob, final_matrix_snp_lip, common_snp = final_matrix()
    #
    # #----- get just the sample part of the matrices and plot them
    snp_count_an, counts_an = extract_counts_df(final_matrix_snp_an, 'an.csv')
    snp_count_ob, counts_ob = extract_counts_df(final_matrix_snp_ob, 'ob.csv')
    snp_count_lip, counts_lip = extract_counts_df(final_matrix_snp_lip, 'lip.csv')
    snp_count_an, counts_an = add_counts_non_zero_df(snp_count_an)
    snp_count_ob, counts_ob = add_counts_non_zero_df(snp_count_ob)
    snp_count_lip, counts_lip = add_counts_non_zero_df(snp_count_lip)
    #
    #
    # #----- calculate the genotype frequency, the allele frequency and check the Hardy-Weinberg equilibrium
    n_sample_an, snp_genotype_freq_an = genotype_frequency(snp_count_an, 'AN')
    n_sample_ob, snp_genotype_freq_ob = genotype_frequency(snp_count_ob, 'OB')
    n_sample_lip, snp_genotype_freq_lip = genotype_frequency(snp_count_lip, 'LIP')
    snp_allele_freq_an = allele_frequency(final_matrix_snp_an, 'an.csv')
    snp_allele_freq_ob = allele_frequency(final_matrix_snp_ob, 'ob.csv')
    snp_allele_freq_lip = allele_frequency(final_matrix_snp_lip, 'lip.csv')
    #.
    #----------previouse hw check and chi test, not done for tirana
    check_H_W(snp_allele_freq_an, snp_genotype_freq_an, n_sample_an, 'expected_an.csv')
    check_H_W(snp_allele_freq_ob, snp_genotype_freq_ob, n_sample_ob, 'expected_ob.csv')
    check_H_W(snp_allele_freq_lip, snp_genotype_freq_lip, n_sample_lip, 'expected_lip.csv')
    #----------
    #
    #final matrix
    freq_all_matrix()

    #chi square test and filtering at 0.05
    chi_square_test('final_freq_matrix_LIP_rec.csv')
    chi_square_test('final_freq_matrix_LIP_dom.csv')
    filter_chitest('chitest_final_freq_matrix_LIP_dom')
    filter_chitest('chitest_final_freq_matrix_LIP_rec')


    #-----------association study
    snps_an = pd.read_csv('./SNP_MATRIX/COMMON_SNP_MATRIX/FINAL_MATRIX_SNP_ANORESSIA.csv',sep=',',header=0, index_col='Unnamed: 0')
    chitest_passed_dom = pd.read_csv('./chitest_final_freq_matrix_LIP_dom_passed.csv',sep=',',header=0)
    chitest_passed_rec = pd.read_csv('./chitest_final_freq_matrix_LIP_rec_passed.csv',sep=',',header=0)
    chitest_passed_rec = chitest_passed_rec['Unnamed: 0'].to_list()
    chitest_passed_dom = chitest_passed_dom['Unnamed: 0'].to_list()
    print('----------association')
    print(len(chitest_passed_dom))
    print(len(chitest_passed_rec))
    snp_passed = list(set(chitest_passed_dom) | set(chitest_passed_rec))
    print(len(snp_passed), 'snp_passed')
    snps_ob = pd.read_csv('./SNP_MATRIX/COMMON_SNP_MATRIX/FINAL_MATRIX_SNP_OBESITY.csv',sep=',',header=0, index_col='Unnamed: 0')
    snps_lip = pd.read_csv('./SNP_MATRIX/COMMON_SNP_MATRIX/FINAL_MATRIX_SNP_LIPEDEMA.csv',sep=',',header=0, index_col='Unnamed: 0')

    an_passed, ob_passed, lip_passed = filter_chitest_final_matrix(snps_an, snps_ob, snps_lip, snp_passed)
    #print(lip_passed.shape)
    controls = pd.merge(ob_passed, an_passed, on=['dbsnp', '#CHROM', 'POS'], how='outer')
    #print(controls.shape)
    if not os.path.exists('./LIPEDEMA__OBESITY-ANORESSIA'):
        os.mkdir('./LIPEDEMA__OBESITY-ANORESSIA')

    ped_map(lip_passed, controls, './LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA')

    ##plink
    #QC
    os.system("/home/elisa/Desktop/plink/plink-1.07-x86_64/plink --file LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA --noweb --no-fid --no-parents --map3 --geno 0.02 --mind 0.02 --check-sex --maf 0.05 --hwe 1e-10 --make-bed --write-snplist --out LIPEDEMA__OBESITY-ANORESSIA/anoressia_qc")

    #--assoc: case control test
    os.system("/home/elisa/Desktop/plink/plink-1.07-x86_64/plink --noweb --no-fid --no-parents  --map3 --file LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA --make-bed --out LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA")
    os.system("/home/elisa/Desktop/plink/plink-1.07-x86_64/plink --noweb --no-fid --no-parents  --map3 --file LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA  --assoc --adjust --sex --ci 0.95 --out LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA")

    #filter pvalues<0.05
    assoc = pd.read_csv('./LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA.assoc', sep='\s+')
    adjusted = pd.read_csv('./LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA.assoc.adjusted', sep='\s+')

    lip_results = pd.merge(assoc, adjusted, on=["CHR", "SNP"], how = "outer")
    lip_results[["SNP", "OR", "UNADJ","BONF"]].to_csv('./lipedema_association_results.csv')
    lip_results = lip_results[(lip_results['BONF']<=0.05)]
    an_passed_filt, ob_passed_filt, lip_passed_filt = filter_chitest_final_matrix(snps_an, snps_ob, snps_lip, lip_results['SNP'])
    controls = pd.merge(ob_passed_filt, an_passed_filt, on=['dbsnp', '#CHROM', 'POS'], how='outer')
    print(controls.shape)
    ped_map(lip_passed_filt, controls, './LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA_assoc_filter')

    os.system("/home/elisa/Desktop/plink/plink-1.07-x86_64/plink --noweb --no-fid --no-parents  --map3 --file LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA_assoc_filter  --model --out LIPEDEMA__OBESITY-ANORESSIA/FINAL_MATRIX_SNP_LIPEDEMA__OBESITY-ANORESSIA_assoc_filter")

    snp_allele_freq_lip = pd.DataFrame(allele_frequency(lip_passed, 'lip_passed.csv'))

    snp_allele_freq_contr = pd.DataFrame(allele_frequency(pd.merge(ob_passed, an_passed, on=['dbsnp', '#CHROM', 'POS'], how='outer'), 'controls.csv'))


    for snp in lip_results['SNP'].to_list():
        print(snp)
        #freq_matrx[snp] = pd.concat(snp_allele_freq_an.loc['0/n', snp], snp_allele_freq_contr.loc['0/n', snp],)
        print('0/n', round(snp_allele_freq_lip.loc['0/n', snp], 3), round(snp_allele_freq_contr.loc['0/n', snp], 3))
        print('1/n',round(snp_allele_freq_lip.loc['1/n', snp], 3), round(snp_allele_freq_contr.loc['1/n', snp], 3))
